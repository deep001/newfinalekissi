<?php
require_once __DIR__ . '/../../autoload/define.php';
session_start();
use App\Classes\Config;
?>

<script src="assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
<script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

<script src="assets/vendor/hs-header/dist/hs-header.min.js"></script>
<script src="assets/vendor/hs-go-to/dist/hs-go-to.min.js"></script>
<script src="assets/vendor/hs-unfold/dist/hs-unfold.min.js"></script>
<script src="assets/vendor/hs-mega-menu/dist/hs-mega-menu.min.js"></script>
<script src="assets/vendor/hs-show-animation/dist/hs-show-animation.min.js"></script>
<script src="assets/vendor/hs-sticky-block/dist/hs-sticky-block.min.js"></script>
<script src="assets/vendor/hs-counter/dist/hs-counter.min.js"></script>
<script src="assets/vendor/appear.js"></script>
<script src="assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<script src="assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="assets/vendor/aos/dist/aos.js"></script>
<script src="assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>

<script src="assets/js/hs.core.js"></script>
<script src="assets/js/hs.validation.js"></script>
<script src="assets/js/hs.cubeportfolio.js"></script>
<script src="assets/js/hs.slick-carousel.js"></script>
<script src="assets/js/hs.fancybox.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript">
		$(document).ready(function(){
			   $("#exampleModal").show();
		});
	</script>-->
<script>
        $(document).on('ready', function () {
            // initialization of header
            var header = new HSHeader($('#header')).init();

            // initialization of mega menu
            var megaMenu = new HSMegaMenu($('.js-mega-menu'), {
                desktop: {
                    position: 'left'
                }
            }).init();

            // initialization of fancybox
            $('.js-fancybox').each(function () {
              var fancybox = $.HSCore.components.HSFancyBox.init($(this));
            });

            // initialization of unfold
            var unfold = new HSUnfold('.js-hs-unfold-invoker').init();

            // initialization of slick carousel
            $('.js-slick-carousel').each(function() {
                var slickCarousel = $.HSCore.components.HSSlickCarousel.init($(this));
            });

            // initialization of form validation
            $('.js-validate').each(function() {
                $.HSCore.components.HSValidation.init($(this), {
                    rules: {
                        confirmPassword: {
                            equalTo: '#signupPassword'
                        }
                    }
                });
            });

            // initialization of show animations
            $('.js-animation-link').each(function () {
                var showAnimation = new HSShowAnimation($(this)).init();
            });

            // initialization of counter
            $('.js-counter').each(function() {
                var counter = new HSCounter($(this)).init();
            });

            // initialization of sticky block
            var cbpStickyFilter = new HSStickyBlock($('#cbpStickyFilter'));

            // initialization of cubeportfolio
            $('.cbp').each(function () {
                var cbp = $.HSCore.components.HSCubeportfolio.init($(this), {
                    layoutMode: 'grid',
                    filters: '#filterControls',
                    displayTypeSpeed: 0
                });
            });

            $('.cbp').on('initComplete.cbp', function() {
                // update sticky block
                cbpStickyFilter.update();

                // initialization of aos
                AOS.init({
                    duration: 650,
                    once: true
                });
            });

            $('.cbp').on('filterComplete.cbp', function() {
                // update sticky block
                cbpStickyFilter.update();

                // initialization of aos
                AOS.init({
                    duration: 650,
                    once: true
                });
            });

            $('.cbp').on('pluginResize.cbp', function() {
                // update sticky block
                cbpStickyFilter.update();
            });

            // animated scroll to cbp container
            $('#cbpStickyFilter').on('click', '.cbp-filter-item', function (e) {
                $('html, body').stop().animate({
                    scrollTop: $('#demoExamplesSection').offset().top
                }, 200);
            });

            // initialization of go to
            $('.js-go-to').each(function () {
                var goTo = new HSGoTo($(this)).init();
            });
        });
    </script>

<script>
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) document.write('<script src="assets/vendor/polifills.js"><\/script>');
    </script>
<!--	<link rel="stylesheet" type="text/css" href="bootstrap/css/animate.css" />-->
<!--<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />-->
<!--<link rel="stylesheet" type="text/css" href="bootstrap/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="bootstrap/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="bootstrap/css/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="bootstrap/css/prettyPhoto.css" />
<link rel="stylesheet" type="text/css" href="bootstrap/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="bootstrap/css/theme.css" /> -->

<!--<script type="text/javascript" src="bootstrap/js/jquery-1.11.0.min.js"></script>-->

	<script>
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
</script>
	
	<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">Open modal for @fat</button>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Open modal for @getbootstrap</button>
...more buttons... 
 -->