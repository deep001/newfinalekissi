<?php 

namespace App\Classes;


abstract class Audioabstr
{
    abstract protected function getAudioRecord();
	abstract protected function getChannelTwoAudioRecord();
	abstract protected function getChannelThreeAudioRecord();
	abstract protected function getChannelfourthAudioRecord();
	abstract protected function getAudioRecordCompleteDetail();
	abstract protected function getChannelfifthBooksList();
	abstract protected function getChannelfifthBooksListasc();
}