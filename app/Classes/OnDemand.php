<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();


class OnDemand extends Ondemandabstr
{
	
	
	 
    use Databasetraits;
  
	protected $firstondemandcategory = 'JAMES BOND';
	protected $secondondemandcategory = 'MOST POPULAR MOVIES';
	protected $thirdondemandcategory = 'TOP TV SERIES';
	protected $fourthondemandcategory = 'NEW MOVIES THIS MONTH';
	protected $fifthondemandcategory = 'STREAMS';
	protected $sixthondemandcategory = 'LAST ADDED SERIES';
	
	
   
   public function getFirstOnDemand()
	{   
	   
		
		  $this->db->query("select ond.ondemandimage,ond.ondemandid from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid  and ondc.ondemandcategoryname = :ondemandcname order by ondemandid DESC");
           $exe =  $this->db->execute(array(
        ":ondemandcname" => $this->firstondemandcategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	} 
	 public function getSecondOnDemand()
	{   
	   
		
		  $this->db->query("select ond.ondemandimage,ond.ondemandid from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid  and ondc.ondemandcategoryname = :ondemandcname order by ondemandid DESC");
           $exe =  $this->db->execute(array(
        ":ondemandcname" => $this->secondondemandcategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	 public function getThirdOnDemand()
	{   
	   
		
		  $this->db->query("select ond.ondemandimage,ond.ondemandid from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid  and ondc.ondemandcategoryname = :ondemandcname order by ondemandid DESC");
           $exe =  $this->db->execute(array(
        ":ondemandcname" => $this->thirdondemandcategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	} 
	 public function getFourthOnDemand()
	{   
	   
		
		  $this->db->query("select ond.ondemandimage,ond.ondemandid from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid  and ondc.ondemandcategoryname = :ondemandcname order by ondemandid DESC");
           $exe =  $this->db->execute(array(
        ":ondemandcname" => $this->fourthondemandcategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	} 
	 public function getFifthOnDemand()
	{   
	   
		
		  $this->db->query("select ond.ondemandimage,ond.ondemandid from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid  and ondc.ondemandcategoryname = :ondemandcname order by ondemandid DESC");
           $exe =  $this->db->execute(array(
        ":ondemandcname" => $this->fifthondemandcategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	} 
	 public function getSixthOnDemand()
	{   
	   
		
		  $this->db->query("select ond.ondemandimage,ond.ondemandid from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid  and ondc.ondemandcategoryname = :ondemandcname order by ondemandid DESC");
           $exe =  $this->db->execute(array(
        ":ondemandcname" => $this->sixthondemandcategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	} 
/*	public function getChannelTwoAudioRecord()
	{
		$this->db->query("select au.audioimage from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid DESC");
           $exe =  $this->db->execute(array(
        ":audiocname" => $this->secondaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	 public function getAudioRecord()
	  { 
		
     $this->db->query("select au.*,auc.* from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid order by eventid DESC");
     $exe =  $this->db->execute(array(
        ":USERMAIL" => $_SESSION['u_email'],
		":ID"       => $_SESSION['uid']
         
    ));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();
     return $row;
      }
	}  */
   public function getFullDetail($ondemandid)
	{
		$this->db->query("select ondemandimage,ondemandname,ondemandiframe from ondemand where ondemandid=:ID");
           $exe =  $this->db->execute(array(
        ":ID" => $ondemandid
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	
	public function getFirstOnDemandCompleteDetail()
	{   
	   
		
		  $this->db->query("select * from ondemand as ond INNER JOIN ondemandcategory as ondc ON ond.ondemandcategoryid = ondc.ondemandcategoryid order by ondemandid DESC");
           $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	public function getOnDemandCompleteInfo($ondemandid)
	{
		$this->db->query("select * from ondemand where ondemandid=:ID");
           $exe =  $this->db->execute(array(
        ":ID" => $ondemandid
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	
	public function updateondemanddetail($post)
	{
		 	 $ondemandiframe = addslashes($post['ondemandiframe']);
		 $channelname = $post['channelname'];
		 $ondemandname = addslashes(trim(filter_var($post['ondemandname'], FILTER_SANITIZE_STRING)));
		 $ondemanddesc = addslashes(trim(filter_var($post['ondemanddesc'], FILTER_SANITIZE_STRING)));
	
		 $ondemandimage			     = $_FILES['ondemandimage']['name'];
	     $ondemandTemp 		     = $_FILES['ondemandimage']['tmp_name'];
	     $ondemandtype             = $_FILES['ondemandimage']['type'];
	     $ondemandsize             = $_FILES['ondemandimage']['size'];
		 $ondemandid       = $post['ondemandid'];
		 
	    
		 if (!Validation::validateTitle($ondemandname)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid OnDemand Channel name"
                ];
              
            }
		 else if (!Validation::validateTitle($ondemanddesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid OnDemand Description"
                ];
              
            }
		 
			else if(!empty($ondemandimage)){  
				
			

  
				
		
             $this->db->query("UPDATE `ondemand` set `ondemandcategoryid`=:CHNAME,`ondemandname`=:LVNAME,`ondemandimage`=:LVIMAGE,`ondemanddescription`=:LVDESC,`ondemandiframe`=:LVFRAME WHERE `ondemandid`=:ID");
				$update = $this->db->execute(array(
                    ':CHNAME' => $channelname,
					 ':LVNAME' => $ondemandname,
					 ':LVIMAGE' => $ondemandimage,
					 ':LVDESC' => $ondemanddesc,
					 ':LVFRAME' => $ondemandiframe,
					 ':ID' => $ondemandid,
                ));
				
				
				
			}
		
		 if($update)
			  {
					 $pathfirst = '../images/ondemandimages/';
					  // move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					 move_uploaded_file( $ondemandTemp,$pathfirst.$ondemandimage);
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'id' => $ondemandid,
                    
                ];
			  }
		else {  
				
			
                             $this->db->query("UPDATE `ondemand` set `ondemandcategoryid`=:CHNAME,`ondemandname`=:LVNAME,`ondemanddescription`=:LVDESC,`ondemandiframe`=:LVFRAME WHERE `ondemandid`=:ID");
				$update = $this->db->execute(array(
                    ':CHNAME' => $channelname,
					 ':LVNAME' => $ondemandname,
					 ':LVDESC' => $ondemanddesc,
					 ':LVFRAME' => $ondemandiframe,
					 ':ID' => $ondemandid,
                ));
  
				
		
             
			}
			
		 if($update)
			  {
					// $pathfirst = '../images/liveimages';
					  // move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					// move_uploaded_file( $liveTemp,$pathfirst.$liveimage);
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'id' => $ondemandid,
                    
                ];
			  }
			
			
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
	
	
}