<?php 

namespace App\Classes;
use App\Traits\Databasetraits;

session_start();


class Audio extends Audioabstr
{
	
	
	 
    use Databasetraits;
  
	protected $firstaudiocategory = 'Music';
	protected $secondaudiocategory = 'Sports';
	protected $thirdaudiocategory = 'News';
	protected $fourthaudiocategory = 'Movies';
	protected $fifthaudiocategory = 'Books';

	
	
   
   public function getAudioRecord()
	{   
	   
		
		  $this->db->query("select au.audioimage,au.audioid,au.audioname,au.audiodescription,au.audioiframe,au.videoduration from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid DESC");
          $exe =  $this->db->execute(array(
          ":audiocname" => $this->firstaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	} 
	public function getChannelTwoAudioRecord()
	{
		$this->db->query("select au.audioimage,au.audioid,au.audioname,au.audiodescription,au.audioiframe,au.videoduration from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid DESC");
        $exe =  $this->db->execute(array(
        ":audiocname" => $this->secondaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	public function getChannelThreeAudioRecord()
	{
		$this->db->query("select au.audioimage,au.audioid,au.audioname,au.audiodescription,au.audioiframe,au.videoduration from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid DESC");
           $exe =  $this->db->execute(array(
        ":audiocname" => $this->thirdaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	public function getChannelfourthAudioRecord()
	{
		$this->db->query("select au.audioimage,au.audioid,au.audioname,au.audiodescription,au.audioiframe,au.videoduration from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid DESC");
           $exe =  $this->db->execute(array(
        ":audiocname" => $this->fourthaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	public function getChannelfifthBooksList()
	{
		$this->db->query("select au.audioimage,au.audioid,au.audioname,au.audiodescription,au.price,au.audioiframe,au.videoduration from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid DESC");
           $exe =  $this->db->execute(array(
        ":audiocname" => $this->fifthaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	public function getChannelfifthBooksListasc()
    {
		$this->db->query("select au.audioimage,au.audioid,au.audioname,au.audiodescription,au.price,au.audioiframe,au.videoduration from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid  and auc.audiocategoryname = :audiocname order by audioid ASC");
           $exe =  $this->db->execute(array(
        ":audiocname" => $this->fifthaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	
	public function getBooksCompleteInfo($productid)
	{
		$this->db->query("select * from audio where audioid=:ID");
           $exe =  $this->db->execute(array(
        ":ID" => $productid
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
/*	 public function getAudioRecord()
	  { 
		
     $this->db->query("select au.*,auc.* from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid order by eventid DESC");
     $exe =  $this->db->execute(array(
        ":USERMAIL" => $_SESSION['u_email'],
		":ID"       => $_SESSION['uid']
         
    ));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();
     return $row;
      }
	}  */
	
	public function getFullDetail($audioid)
	{
		$this->db->query("select audioimage,audioname,audioiframe from audio where audioid=:ID");
           $exe =  $this->db->execute(array(
        ":ID" => $audioid
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	
	
	public function getAudioRecordCompleteDetail()
	{
		$this->db->query("select * from audio as au INNER JOIN audiocategory as auc ON au.audiocategoryid = auc.audiocategoryid and auc.audiocategoryname != :audiocname order by audioid DESC");
           $exe =  $this->db->execute(array(
        ":audiocname" => $this->fifthaudiocategory
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	
	public function getAudioCompleteInfo($audioid)
	{
		$this->db->query("select * from audio where audioid=:ID");
           $exe =  $this->db->execute(array(
        ":ID" => $audioid
		  ));
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetch();
           return $row;
	  }
	}
	
	public function updateaudiodetail($post)
	{
		 $audioiframe = addslashes($post['audioiframe']);
		 $channelname = $post['channelname'];
		 $audioname = addslashes(trim(filter_var($post['audioname'], FILTER_SANITIZE_STRING)));
		 $audiodesc = addslashes(trim(filter_var($post['audiodesc'], FILTER_SANITIZE_STRING)));
	
		 $audioimage			     = $_FILES['audioimage']['name'];
	     $audioTemp 		     = $_FILES['audioimage']['tmp_name'];
	     $audiotype             = $_FILES['audioimage']['type'];
	     $audiosize             = $_FILES['audioimage']['size'];
		 $audioid       = $post['audioid'];
		 
	    
		 if (!Validation::validateTitle($audioname)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid audio name"
                ];
              
            }
		 else if (!Validation::validateTitle($audiodesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Audio Description"
                ];
              
            }
		 
			else if(!empty($audioimage)){  
				
			

  
				
		
             $this->db->query("UPDATE `audio` set `audiocategoryid`=:CHNAME,`audioname`=:LVNAME,`audioimage`=:LVIMAGE,`audiodescription`=:LVDESC,`audioiframe`=:LVFRAME WHERE `audioid`=:ID");
				$update = $this->db->execute(array(
                    ':CHNAME' => $channelname,
					 ':LVNAME' => $audioname,
					 ':LVIMAGE' => $audioimage,
					 ':LVDESC' => $audiodesc,
					 ':LVFRAME' => $audioiframe,
					 ':ID' => $audioid,
                ));
				
				
				
			}
		
		 if($update)
			  {
					 $pathfirst = 'assets/img/122x183/';
					  // move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					 move_uploaded_file( $audioTemp,$pathfirst.$audioimage);
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'id' => $audioid,
                    
                ];
			  }
		else {  
				
			

  
				
		
              $this->db->query("UPDATE `audio` set `audiocategoryid`=:CHNAME,`audioname`=:LVNAME,`audiodescription`=:LVDESC,`audioiframe`=:LVFRAME WHERE `audioid`=:ID");
				$update = $this->db->execute(array(
                    ':CHNAME' => $channelname,
					 ':LVNAME' => $audioname,
					 ':LVDESC' => $audiodesc,
					 ':LVFRAME' => $audioiframe,
					 ':ID' => $audioid,
                ));
			}
			
		 if($update)
			  {
					// $pathfirst = '../images/liveimages';
					  // move_uploaded_file($photogalleryimageTemp,$photogallerypath.$photogalleryimageRandom);
					// move_uploaded_file( $liveTemp,$pathfirst.$liveimage);
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'id' => $audioid,
                    
                ];
			  }
			
			
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }

  
	


}