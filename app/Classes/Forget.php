<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Forget
{
	// protected $useremail = "ndeepak48@gmail.com";
    // protected $last = "Nautiyal";

   // const LOGIN_STATUS_PENDING = "PENDING";
    // const LOGIN_STATUS_SUCCESS = "SUCCESS";
    // const LOGIN_STATUS_FAILED = "FAILED";

    use Databasetraits;
  
    
     
   /*   private function verifyEmailnPassword($userEmail, $userPassword)
    {
        if (isset($userEmail) && isset($userPassword)) {
            $userObj = new Users();
            $userMeta = $userObj->getUserMetaByEmail($userEmail);
          if ($userMeta) {
                $passwordHash = $userMeta->password;
                
                 
                if (password_verify($userPassword, $passwordHash)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

 
*/


    function getSignInRequest($userEmail, $userPassword)
    {
        if (isset($userEmail) && isset($userPassword)) {
                $userEmail = trim(strtolower(filter_var($userEmail, FILTER_SANITIZE_EMAIL)));
                $userPassword = trim(filter_var($userPassword, FILTER_SANITIZE_STRING));
          //  $gRecaptchaResponse = trim(filter_var($gRecaptchaResponse, FILTER_SANITIZE_STRING));

       /*     $googleObj = new Googlelib();
            if (!$googleObj->gRecaptchaValidate($gRecaptchaResponse)) {
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid googleRecaptchaValidate recaptcha, Please try again"
                ];
                  
            } */
				 if (!Validation::validateEmail($userEmail)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid email"
                ];
              
            } else if (!Validation::validatePasswordStrength($userPassword)) {

                return (object)[
                    'status'=>false,
                    'msg'=>"invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*"
                ];
              
            } 
			/*else if (!$this->verifyEmailnPassword($userEmail, $userPassword)) {
             
                return (object)[
                    'status'=>false,
                    'msg'=>"incorrect email or password"
                ];
              
            }*/
			else { 
				
			

                $this->db->query("select id,email_id from admin where email_id=:EMAIL and password=:PASSWORD"); 
                 $this->db->execute(array(
                    ":EMAIL" => $userEmail,
					 "PASSWORD" => $userPassword,
                   
                ));
                if ($this->db->rowCount() > 0) {
                    $row = $this->db->fetch();
                  return (object)[
                        'status'=>true,
                        'email'=>$row->email_id,
                        'id'=>$row->id,

                    ];  
				//	return $row;
					
                    
                }else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid email id or password ! please try again"
                ];
                }
            }
        } else {
           return Errorlist::errorResponse(false, "incorrect email or password");
        }
    } 

	/* public function getSellerRecord()
	  { 
	      
	    
   
 
	
      
     $this->db->query("select * from sellparkinguser where email=:EMAIL AND Lastname=:last");
   $exe =  $this->db->execute(array(
        ":EMAIL" => $this->useremail,
         ":last" => $this->last
    ));
     if ($this->db->rowCount() > 0) {
     $row = $this->db->fetchAll();

   
   return $row;
     
     }
	 
} */
	  public function forgetPassword($userEmail,$password)
	  {
		       $userEmail = trim(strtolower(filter_var($userEmail, FILTER_SANITIZE_EMAIL)));
               $userPassword = trim(filter_var($password, FILTER_SANITIZE_STRING));
		       if (!Validation::validateEmail($userEmail)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid email"
                ];
              
            } else if (!Validation::validatePasswordStrength($userPassword)) {

                return (object)[
                    'status'=>false,
                    'msg'=>"invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*"
                ];
              
            }
		  else
		  {
			  $this->db->query("update admin set password=:PASS where email_id=:EMAIL");
			  $update =  $this->db->execute(array(
                    ":EMAIL" => $userEmail,
					 "PASS" => $userPassword,
                   
                ));
			  if($update)
			  {
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully update the password"
                ];
			  }
			  else
			  {
				  return(object)[
					  'status'=>false,
					  'msg'=>"failed!try again"
					  ];
			  }
				  
			  
		  }
		  
		        
		       
	  }

}