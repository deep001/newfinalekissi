<?php 

namespace App\Classes;
use App\Traits\Databasetraits;


class Event
{
	

    use Databasetraits;
  
    
	public function getEvent()
	{
		
		  $this->db->query("select * from events order by eventid ASC");
           $exe =  $this->db->execute();
           if ($this->db->rowCount() > 0) {
           $row = $this->db->fetchAll();
           return $row;
	  }
	}
	public function getEventContent($eventid)
	{
		$this->db->query("select * from events WHERE eventid=:EID");
		$exe = $this->db->execute(array(
		':EID' => $eventid,
		));
		if($this->db->rowCount()>0)
		{
			$row = $this->db->fetch();
			return $row;
		}
	}
		public function updateEventDetail($eventid,$Eventheading,$Eventdesc)
	{
		if (isset($eventid) && isset($Eventheading)  && isset($Eventdesc)) {
               $Eventheading = trim(strtolower(filter_var($Eventheading, FILTER_SANITIZE_STRING)));
			$Eventdesc = trim(strtolower(filter_var($Eventdesc, FILTER_SANITIZE_STRING)));
			
        
		 if (!Validation::validateEventHead($Eventheading)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Event Heading"
                ];
              
            }
			else if (!Validation::validateEventDescription($Eventdesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Event Description"
                ];
              
            }
		
		
		else {  
				
			

  
				
		
             $this->db->query("UPDATE `events` set `eventhead`=:EVENTHEAD,`evenddesc`=:EVENTDESC WHERE `eventid`=:EID");
				$update = $this->db->execute(array(
                    ':EID' => $eventid,
					 ':EVENTHEAD' => $Eventheading,
					':EVENTDESC' => $Eventdesc
					
                ));
			
				 if($update)
			  {
					
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Upated",	
                    'eventreturnid' => $eventid,
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Updation ! Please try again"
                ];
                }
            }
		
		}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid heading or content ! please try again"
                ];
                }
	}
	
	public function deleteEventList($eventid)
	{
		$this->db->query("DELETE FROM `events` WHERE eventid=:EID");
		$exe = $this->db->execute(array(
		':EID' => $eventid,
		));
		if($exe)
		{
			return(object)[
				  'status'=>true,
				  'msg'=>"successfully delete Selected Event"
				];
		}
		else {
			return(object)[
				   'status'=>false,
				   'msg'=>"failed ! try again"
				];
		}
			
	}
	public function addEventDetail($Eventheading,$Eventdesc)
	{
			if (isset($Eventheading)  && isset($Eventdesc)) {
				 $Eventheading = trim(strtolower(filter_var($Eventheading, FILTER_SANITIZE_STRING)));
				$Eventdesc = trim(strtolower(filter_var($Eventdesc, FILTER_SANITIZE_STRING)));
		
			 if (!Validation::validateEventHead($Eventheading)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Event Heading"
                ];
              
            }
			else if (!Validation::validateEventDescription($Eventdesc)) {
          
                return (object)[
                    'status'=>false,
                    'msg'=>"invalid Event Description"
                ];
              
            }
				else
				{
					$this->db->query("INSERT INTO `events`(`eventhead`,`evenddesc`,`serial`) VALUES (:EVENTHEAD,:EVENTDESC,'')");
					
				$insert = $this->db->execute(array(
                   ':EVENTHEAD' => $Eventheading,
					':EVENTDESC' => $Eventdesc
                ));
			
				 if($insert)
			  {
					
				    return (object)[
                    'status'=>true,
                    'msg'=>"successfully Added to Events",	
                    
                    
                ];
			  }
              else{
                   // return FALSE;
					 return (object)[
                    'status'=>false,
                    'msg'=>"Failed Addition ! Please try again"
                ];
                }
				}
				
					
			}
		else{
                  
					 return (object)[
                    'status'=>false,
                    'msg'=>"invalid Heading or Description ! please try again",
                ];
                }
	}
	
}