<?php
require_once __DIR__ . '/autoload/define.php';
session_start();
use App\Classes\Config;

?>
<!DOCTYPE html>
<html lang="en">
<head>

<title> Privacy policy | Ocean TV </title>

<?php include_once Config::path()->INCLUDE_PATH.'/oceanfronthead.php'; ?>
</head>
<body>

<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontheadernew.php'; ?>


<main id="content">
<div class="bg-gray-1100 pb-6">
<div class="container px-md-5">
<nav aria-label="breadcrumb">
<ol class="breadcrumb dark pt-5">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item"><a href="#">Terms</a></li>
<!--<li class="breadcrumb-item active" aria-current="page">Vikings</li>-->
</ol>
</nav>
</div>
<div>
<div class="position-relative bg-single-player mb-3">
<img class="img-fluid w-100 h-393rem object-fit-cover" src="assets/img/970x550/img21.jpg" alt="Image-Description">
</div>
</div>
<div class="container px-md-5">
<div class="d-md-flex align-items-center justify-content-between mb-5">
<div>
<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">About us</h6>
<!--<div class="nav nav-meta mb-3 font-size-1">
<div>
<a href="#" class="text-gray-1300">Action</a>
<span class="text-gray-1300">,</span>
<a href="#" class="text-gray-1300">Adventures</a>
<span class="text-gray-1300">,</span>
<a href="#" class="text-gray-1300">Drama</a>
</div>
<span class="text-gray-1300 mx-2 px-1">|</span>
<div class="text-gray-1300">2020</div>
</div>-->
</div>
<!--<div class="d-flex align-items-center">
<div class="d-flex">
<div>
<i class="fas fa-star text-primary font-size-42"></i>
</div>
<div class="text-lh-1 ml-1">
<div class="text-primary font-size-24 font-weight-semi-bold">10.0</div>
<span class="text-gray-1300 font-size-12">1 Vote</span>
</div>
</div>
<div class="d-flex align-items-center ml-5 text-gray-1300">
<div>
<i class="far fa-heart font-size-30"></i>
</div>
<a href="#" class="text-gray-1300 ml-2">+ Playlist</a>
</div>
</div>
</div>-->
<!--<div class="d-flex align-items-center text-gray-1300 font-secondary mb-4">
<div>
<i class="far fa-eye font-size-18"></i>
<span class="font-size-12 font-weight-semi-bold ml-1">1.6k views</span>
</div>
<div class="ml-6">
<a href="#" class="text-gray-1300">
<i class="far fa-thumbs-up font-size-18"></i>
</a>
<span class="font-size-12 font-weight-semi-bold ml-1">17+</span>
</div>
</div>-->
<!--<div class="d-md-flex justify-content-between">
<p class="mb-md-0 text-gray-1300">New Season 5 just flow in. Watch and Debate</p>
<div>
<div class="text-white">Tags:
<a href="#">Brother</a>
<span>,</span>
<a href="#">Brother</a>
<span>,</span>
<a href="#">Relationship</a>
<span>,</span>
<a href="#">King</a>
<span>,</span>
<a href="#">Vikings</a>
</div>
</div>
</div>-->
</div>
</div>
<!--<div class="bg-gray-3100 py-5 pt-lg-7 pb-lg-6">
<div class="container px-md-5">
<div>
<ul class="nav overflow-auto overflow-md-hidden flex-nowrap flex-md-wrap tab-nav__v15 mb-6 pb-1" id="pills-tab" role="tablist">
<li class="nav-item flex-shrink-0 flex-shrink-md-1">
<a class="nav-link active" id="pills-one-example-tab" data-toggle="pill" href="#pills-one-example" role="tab" aria-controls="pills-one-example" aria-selected="true">SEASON 1</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1 ml-3">
<a class="nav-link" id="pills-two-example-tab" data-toggle="pill" href="#pills-two-example" role="tab" aria-controls="pills-two-example" aria-selected="false">SEASON 2</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1 ml-3">
<a class="nav-link" id="pills-three-example-tab" data-toggle="pill" href="#pills-three-example" role="tab" aria-controls="pills-three-example" aria-selected="false">SEASON 3</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1 ml-3">
<a class="nav-link" id="pills-four-example-tab" data-toggle="pill" href="#pills-four-example" role="tab" aria-controls="pills-four-example" aria-selected="false">SEASON 4</a>
</li>
<li class="nav-item flex-shrink-0 flex-shrink-md-1 ml-3">
<a class="nav-link" id="pills-five-example-tab" data-toggle="pill" href="#pills-five-example" role="tab" aria-controls="pills-five-example" aria-selected="false">SEASON 5</a>
</li>
</ul>
<div class="tab-content dark">
<div class="tab-pane fade show active" id="pills-one-example" role="tabpanel" aria-labelledby="pills-one-example-tab">
<div class="max-width-900rem">
<div class="row mx-n2">
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img31.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E05</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Prisoner</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img32.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E04</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Plan</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img33.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E01</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Departed Part 1</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img34.jpg" alt="Image-Desc">
</a>
</div>
 <a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E02</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Departed Part 2</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img35.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E03</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">Home Land</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-two-example" role="tabpanel" aria-labelledby="pills-two-example-tab">
<div class="max-width-900rem">
<div class="row mx-n2">
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v2.html">
<img class="img-fluid" src="assets/img/970x550/img35.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v2.html">
<span class="text-gray-1300 font-size-12">S05E03</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v2.html" class="d-inline-block">Home Land</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v2.html">
<img class="img-fluid" src="assets/img/970x550/img31.jpg" alt="Image-Desc">
 </a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v2.html">
<span class="text-gray-1300 font-size-12">S05E05</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v2.html" class="d-inline-block">The Prisoner</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v2.html">
<img class="img-fluid" src="assets/img/970x550/img33.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v2.html">
<span class="text-gray-1300 font-size-12">S05E01</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v2.html" class="d-inline-block">The Departed Part 1</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v2.html">
<img class="img-fluid" src="assets/img/970x550/img34.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v2.html">
<span class="text-gray-1300 font-size-12">S05E02</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v2.html" class="d-inline-block">The Departed Part 2</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v2.html">
<img class="img-fluid" src="assets/img/970x550/img32.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v2.html">
<span class="text-gray-1300 font-size-12">S05E04</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v2.html" class="d-inline-block">The Plan</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-three-example" role="tabpanel" aria-labelledby="pills-three-example-tab">
<div class="max-width-900rem">
<div class="row mx-n2">
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v3.html">
<img class="img-fluid" src="assets/img/970x550/img33.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v3.html">
<span class="text-gray-1300 font-size-12">S05E01</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v3.html" class="d-inline-block">The Departed Part 1</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v3.html">
<img class="img-fluid" src="assets/img/970x550/img34.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v3.html">
<span class="text-gray-1300 font-size-12">S05E02</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v3.html" class="d-inline-block">The Departed Part 2</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v3.html">
<img class="img-fluid" src="assets/img/970x550/img35.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v3.html">
<span class="text-gray-1300 font-size-12">S05E03</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v3.html" class="d-inline-block">Home Land</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v3.html">
<img class="img-fluid" src="assets/img/970x550/img32.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v3.html">
<span class="text-gray-1300 font-size-12">S05E04</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v3.html" class="d-inline-block">The Plan</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v3.html">
<img class="img-fluid" src="assets/img/970x550/img31.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v3.html">
<span class="text-gray-1300 font-size-12">S05E05</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v3.html" class="d-inline-block">The Prisoner</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-four-example" role="tabpanel" aria-labelledby="pills-four-example-tab">
<div class="max-width-900rem">
<div class="row mx-n2">
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img31.jpg" alt="Image-Desc">
</a>
</div>
 <a class="d-inline-block" href="../single-episodes/single-episodes-v4.html">
<span class="text-gray-1300 font-size-12">S05E05</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v4.html" class="d-inline-block">The Prisoner</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img32.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v4.html">
<span class="text-gray-1300 font-size-12">S05E04</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v4.html" class="d-inline-block">The Plan</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img33.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v4.html">
<span class="text-gray-1300 font-size-12">S05E01</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v4.html" class="d-inline-block">The Departed Part 1</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img34.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v4.html">
<span class="text-gray-1300 font-size-12">S05E02</span>
</a>
 <div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v4.html" class="d-inline-block">The Departed Part 2</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img35.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v4.html">
<span class="text-gray-1300 font-size-12">S05E03</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v4.html" class="d-inline-block">Home Land</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade" id="pills-five-example" role="tabpanel" aria-labelledby="pills-five-example-tab">
<div class="max-width-900rem">
<div class="row mx-n2">
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img33.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E01</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Departed Part 1</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img34.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E02</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Departed Part 2</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img35.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E03</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">Home Land</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img32.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E04</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Plan</a>
</div>
</div>
</div>
<div class="col-md-4 col-lg px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v1.html">
<img class="img-fluid" src="assets/img/970x550/img31.jpg" alt="Image-Desc">
</a>
</div>
<a class="d-inline-block" href="../single-episodes/single-episodes-v1.html">
<span class="text-gray-1300 font-size-12">S05E05</span>
</a>
<div class="font-size-1 mb-0 product-title font-weight-bold">
<a href="../single-episodes/single-episodes-v1.html" class="d-inline-block">The Prisoner</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>-->
<div class="bg-gray-1100 py-5 pt-lg-8 pb-lg-9">
<div class="container px-md-5">
<div>
<!--<h5 class="text-white font-size-22 font-weight-medium mb-5">You may also like after: Vikings</h5>
<div class="max-width-444rem dark mb-5 mb-lg-10">
<div class="row mx-n2">
<div class="col-md px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img27.jpg" alt="Image-Desc">
</a>
</div>
<div class="d-flex align-items-center">
<div class="product-meta font-size-1">
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">Action</a></span>
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">Comedy</a></span>
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">Crime</a></span>
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">2020</a></span>
</div>
</div>
<div class="font-size-1 mb-0 product-title font-weight-bold d-inline-block">
<a href="../single-episodes/single-episodes-v4.html">Leathel Weapon</a>
</div>
</div>
</div>
<div class="col-md px-2">
<div class="product mb-4 mb-lg-0">
<div class="product-image mb-1">
<a class="d-block position-relative stretched-link" href="../single-episodes/single-episodes-v4.html">
<img class="img-fluid" src="assets/img/970x550/img34.jpg" alt="Image-Desc">
</a>
</div>
<div class="d-flex align-items-center">
<div class="product-meta font-size-1">
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">Comedy</a></span>
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">Drama</a></span>
<span><a href="../single-episodes/single-episodes-v4.html" class="h-g-primary" tabindex="0">2020</a></span>
</div>
</div>
<div class="font-size-1 mb-0 product-title font-weight-bold d-inline-block">
<a href="../single-episodes/single-episodes-v4.html">Defiene</a>
</div>
</div>
</div>
</div>
</div>-->
<div>
<div class="tab-nav__v10 mb-6">
<ul class="nav justify-content-center border-bottom border-gray-5300" role="tablist">
<li class="nav-item">
<a class="nav-link active" id="pills-one-code-features-example1-tab" data-toggle="pill" href="#pills-one-code-features-example1" role="tab" aria-controls="pills-two-code-features-example1" aria-selected="true">Privacy Policy</a>
</li>
<!--<li class="nav-item">
<a class="nav-link" id="pills-one-code-features-example1-tab" data-toggle="pill" href="#pills-two-code-features-example1" role="tab" aria-controls="pills-two-code-features-example1" aria-selected="false">About us</a>
</li>-->
</ul>
</div>
	<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">PRIVACY POLICIES:</h6>
<div class="tab-content">
<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">This privacy policy explains the personal information protection practices of OCEAN OG GRÂCE TV. We are committed to protecting and respecting your privacy. Any personal information that we collect from you, or that you provide to us, will not be used and/or disclosed. Please read this text carefully to understand our views and practices regarding your personal information and how we will treat it. The terms below govern the use of OcéandeGracetv.COM. Your use of this website and its content (defined below) signifies your acceptance of these terms. If you do not agree to these terms, please stop using this website and its content immediately.</p>
<!--<p class="text-gray-1300 line-height-lg mb-0">Sed leo elit, volutpat quis aliquet eu, elementum eget arcu. Aenean ligula tellus, malesuada eu ultrices vel, vulputate sit amet metus. Donec tincidunt sapien ut enim feugiat, sed egestas dolor ornare.</p>-->

</div>
		<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">RIGHT OF OWNERSHIP AND USE OF CONTENT:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">All information and content on this website including but not limited to texts, analyzes, reports, articles, graphics, software applications, audio and video files, trademarks, service marks and commercial graphics (“Content”) constitute the property of Ocean de Grâce TV belong to Euloge Ekissi N’takpé and are the subject of a license. The content of this site is protected on a worldwide basis under applicable intellectual property laws. You are permitted to make and use printed content for internal use only, provided you maintain all copyright, trademark and other proprietary notices therein. Ocean of Grâce TV reserves the right to terminate the right to make copies of the content at any time. It is prohibited to incorporate the content of this site into another website. All rights in content are reserved, with the exception of the limited use rights expressly provided in this paragraph.</p>
<!--<p class="text-gray-1300 line-height-lg mb-0">Sed leo elit, volutpat quis aliquet eu, elementum eget arcu. Aenean ligula tellus, malesuada eu ultrices vel, vulputate sit amet metus. Donec tincidunt sapien ut enim feugiat, sed egestas dolor ornare.</p>-->

</div>
	
	<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">CONNECTIONS:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">We do not endorse websites owned by third parties that are outside of Ocean of Grace. We have not reviewed any of the sites potentially linked to this site and assume no responsibility for them. Use of any link to other sites from this website is at your own risk. By using any link to a site or allowing access to the content of this website, Ocean of Grace TV does not endorse the operator of the site or its content. Subject to the provisions of any service or other applicable agreement, we will remove any link from our website at the request of the owner of the linked site. Please send us any request to remove the link by email to the following address: info.oceandegrace@gmail.com.</p>
<!--<p class="text-gray-1300 line-height-lg mb-0">Sed leo elit, volutpat quis aliquet eu, elementum eget arcu. Aenean ligula tellus, malesuada eu ultrices vel, vulputate sit amet metus. Donec tincidunt sapien ut enim feugiat, sed egestas dolor ornare.</p>-->

</div>
	
	<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">DISCLAIMER OF WARRANTIES:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">You use this website and its content at your own risk. We take care to present accurate and up-to-date information on this site. However, we make no warranty or claim as to the accuracy and timeliness of the content. Content may become inaccurate as a result of changes made after posting to this website. Ocean of Grâce TV assumes no responsibility for updating the content or correcting inaccuracies or errors on this site. The content and operation of this website are provided “as is” and Ocean of Grace disclaims all warranties and claims of any kind, express or implied, including warranties of merchantability, fitness for a particular purpose, of title, non-infringement, accuracy, completeness and timeliness. Some jurisdictions do not allow the waiver of certain warranties. In such cases, we accept no liability to the fullest extent permitted by applicable law.</p>
<!--<p class="text-gray-1300 line-height-lg mb-0">Sed leo elit, volutpat quis aliquet eu, elementum eget arcu. Aenean ligula tellus, malesuada eu ultrices vel, vulputate sit amet metus. Donec tincidunt sapien ut enim feugiat, sed egestas dolor ornare.</p>-->

</div>
	
	<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">YOUR SAFETY RESPONSIBILITY:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">Océan of Grâce TV declines all responsibility for any damage that may result from the accessibility, use or download of the content of this site. If you download or copy any content from this website, you are responsible for taking all necessary precautions to ensure the security and integrity of your computer and systems, such as installing software protection against viruses.</p>

</div>

<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">RESPONSIBILITY:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">Neither Ocean of Grace TV, nor any of its agents, nor any other party participating in the creation, production or operation of this website or its content is liable for direct, incidental, consequential, indirect or punitive or loss of profit, goodwill or data caused by your use of this website or its content, whatever it may be.</p>

</div>

<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">MODIFICATIONS:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">Ocean of Grâce reserves the right to modify from time to time, at any time and without notice, the content of this website and the terms and conditions attached to it. These changes may consist of the replacement of various terms or specific notices. YOU SHOULD CONSULT THESE TERMS OF USE FROM TIME TO TIME TO BE AWARE OF THE CHANGES MADE. Continuing to use of this website constitutes acceptance of any changes made, which are effective the moment they are posted.</p>

</div>

<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">Age limit for children SPECIAL NOTE TO PARENTS:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">Ocean of Grâce does not intentionally collect or solicit data from children under the age of thirteen (13) through this site. Parents should monitor their children’s online activities and consider using parental control tools available from online services and software manufacturers that help provide a child-friendly online environment. These tools can also prevent children from disclosing their name, address and other personal information without parental permission.</p>

</div>

<h6 class="font-size-24 text-white font-weight-medium font-secondary mb-3 mb-md-2">Warning Regarding Forward-looking Statements:</h6>
	<div class="tab-pane fade show active" id="pills-one-code-features-example1" role="tabpanel" aria-labelledby="pills-one-code-features-example1-tab">
<p class="text-gray-1300 line-height-lg" style="text-align:justify;">No inappropriate use of the website will be accepted. You agree not to tamper with the software or any functionality of the Website, not to introduce any material into it that in any way contains a virus, logic bomb, Trojan horse, worms, cancel bot message, saturation attacks or any other programming virus that can damage, hinder, intercept or expropriate a system, data or information. You may not do anything that imposes an unreasonable or disproportionate load on our infrastructure, including, but not limited to, spamming or other type of mass sending of email, or a saturation attack. You are prohibited from using this website to post or transmit material of an unlawful, threatening, libelous, obscene, scandalous, offensive or pornographic nature or any other material that may constitute or condone unlawful acts.</p>

</div>
	
<div class="tab-pane fade" id="pills-two-code-features-example1" role="tabpanel" aria-labelledby="pills-two-code-features-example1-tab">
<div>
<div class="d-md-flex align-items-center justify-content-between mb-3 pb-1 w-100 home-section">
<h6 class="section-title-dark d-block position-relative font-size-18 font-weight-medium overflow-md-hidden m-0 text-white flex-grow-1">Add a review</h6>
</div>
<div class="text-gray-1300 mb-3">You must be
<a href="#">logged in</a> to post a review.
</div>
<div class="d-flex">
<div class="h-50rem w-50rem">
<img class="img-fluid rounded-circle" src="assets/img/36x36/img1.jpg" alt="Image-Description">
</div>
<div class="ml-3">
<div class="mb-2">
<strong class="font-size-1 text-gray-1300">nilofer</strong>
<span class="text-gray-1300 ml-1">March 1, 2020</span>
</div>
<div class="text-gray-1300 font-size-1 mb-2">Awesome</div>
<div class="text-gray-1300 mb-1">
<a href="#" class="text-gray-1300">
<i class="far fa-thumbs-up font-size-18"></i>
</a>
<span class="font-size-12 font-weight-semi-bold ml-1">0</span>
</div>
<div class="form-group d-flex align-items-center justify-content-between font-size-13 text-gray-1300 text-lh-lg text-body mb-0">
<span class="d-block text-gray-6100">
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
<i class="fas fa-star"></i>
</span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</main>


<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontfooter.php'; ?>


<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<button type="button" class="close position-absolute top-0 right-0 z-index-2 mt-3 mr-3" data-dismiss="modal" aria-label="Close">
<svg aria-hidden="true" class="mb-0" width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</button>

<div class="modal-body">
<form class="js-validate">

<div id="login">

<div class="text-center mb-7">
<h3 class="mb-0">Sign In to Vodi</h3>
<p>Login to manage your account.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signinEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-3">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signinPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
 </div>
</div>

<div class="d-flex justify-content-end mb-4">
<a class="js-animation-link small link-underline" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#forgotPassword",
                                        "groupName": "idForm"
                                    }'>Forgot Password?
</a>
</div>
<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign In</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign In with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Do not have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#signup",
                                        "groupName": "idForm"
                                    }'>Sign Up
</a>
</div>
</div>

<div id="signup" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Create your account</h3>
<p>Fill out the form to get started.</p>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" required data-msg="Please enter a valid email address.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" required data-msg="Your password is invalid. Please try again.">
</div>
</div>


<div class="js-form-message mb-4">
<label class="input-label">Confirm Password</label>
<div class="input-group input-group-sm mb-2">
<input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" required data-msg="Password does not match the confirm password.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Sign Up</button>
</div>
<div class="text-center mb-3">
<span class="divider divider-xs divider-text">OR</span>
</div>
<a class="btn btn-sm btn-ghost-secondary btn-block mb-2" href="#">
<span class="d-flex justify-content-center align-items-center">
<i class="fab fa-google mr-2"></i>
Sign Up with Google
</span>
</a>
<div class="text-center">
<span class="small text-muted">Already have an account?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Sign In
</a>
</div>
</div>


<div id="forgotPassword" style="display: none; opacity: 0;">

<div class="text-center mb-7">
<h3 class="mb-0">Recover password</h3>
<p>Instructions will be sent to you.</p>
</div>


<div class="js-form-message">
<label class="sr-only" for="recoverEmail">Your email</label>
<div class="input-group input-group-sm mb-2">
<input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" required data-msg="Please enter a valid email address.">
</div>
</div>

<div class="mb-3">
<button type="submit" class="btn btn-sm btn-primary btn-block">Recover Password</button>
</div>
<div class="text-center mb-4">
<span class="small text-muted">Remember your password?</span>
<a class="js-animation-link small font-weight-bold" href="javascript:;" data-hs-show-animation-options='{
                                        "targetSelector": "#login",
                                        "groupName": "idForm"
                                    }'>Login
</a>
</div>
</div>

</form>
</div>

</div>
</div>
</div>


<aside id="sidebarContent" class="hs-unfold-content sidebar sidebar-left off-canvas-menu">
<div class="sidebar-scroller">
<div class="sidebar-container">
<div class="sidebar-footer-offset" style="padding-bottom: 7rem;">

<div class="d-flex justify-content-end align-items-center py-2 px-4 border-bottom">

<a class="navbar-brand mr-auto" href="../home/index.html" aria-label="Vodi">
<svg version="1.1" width="103" height="40px">
<linearGradient id="vodi-gr1" x1="0%" y1="0%" x2="100%" y2="0%">
<stop offset="0" style="stop-color:#2A4999"></stop>
<stop offset="1" style="stop-color:#2C9CD4"></stop>
</linearGradient>
<g class="vodi-gr">
<path class="vodi-svg0" d="M72.8,12.7c0-2.7,0-1.8,0-4.4c0-0.9,0-1.8,0-2.8C73,3,74.7,1.4,77,1.4c2.3,0,4.1,1.8,4.2,4.2c0,1,0,2.1,0,3.1
                                    c0,6.5,0,9.4,0,15.9c0,4.7-1.7,8.8-5.6,11.5c-4.5,3.1-9.3,3.5-14.1,0.9c-4.7-2.5-7.1-6.7-7-12.1c0.1-7.8,6.3-13.6,14.1-13.2
                                    c0.7,0,1.4,0.2,2.1,0.3C71.3,12.2,72,12.4,72.8,12.7z M67.8,19.8c-2.9,0-5.2,2.2-5.2,5c0,2.9,2.3,5.3,5.2,5.3
                                    c2.8,0,5.2-2.4,5.2-5.2C73,22.2,70.6,19.8,67.8,19.8z
                                    M39.9,38.6c-7.3,0-13.3-6.1-13.3-13.5c0-7.5,5.9-13.4,13.4-13.4c7.5,0,13.4,6,13.4,13.5
                                    C53.4,32.6,47.4,38.6,39.9,38.6z M39.9,30.6c3.2,0,5.6-2.3,5.6-5.6c0-3.2-2.3-5.5-5.5-5.5c-3.2,0-5.6,2.2-5.6,5.4
                                    C34.4,28.2,36.7,30.6,39.9,30.6z
                                    M14.6,27c0.6-1.4,1.1-2.6,1.6-3.8c1.2-2.9,2.5-5.8,3.7-8.8c0.7-1.7,2-2.8,4-2.7c3,0,4.9,2.6,3.8,5.4
                                    c-0.5,1.3-1.2,2.6-1.8,3.9c-2.4,5-4.9,10-7.3,15c-0.8,1.6-2,2.6-3.9,2.6c-2,0-3.3-0.8-4.2-2.6c-2.7-5.6-5.3-11.1-8-16.7
                                    c-0.3-0.7-0.6-1.3-0.9-2c-0.8-1.8-0.3-3.7,1.1-4.8c1.5-1.2,4-1.3,5.3,0c0.7,0.6,1.2,1.5,1.6,2.3C11.3,18.8,12.9,22.7,14.6,27z
                                    M90.9,25.1c0,3.1,0,6.2,0,9.4c0,1.9-1.2,3.4-2.9,4c-1.7,0.5-3.5,0-4.5-1.6c-0.5-0.8-0.8-1.8-0.8-2.6
                                    c-0.1-6.1-0.1-11.3,0-17.5c0-2.2,1.5-3.9,3.5-4.2c2.1-0.3,4.1,0.9,4.7,2.9c0.1,0.5,0.2,1.1,0.2,1.6C90.9,20,90.9,22.1,90.9,25.1
                                    C90.9,25.1,90.9,25.1,90.9,25.1z
                                    M90.2,4.7L86,2.3c-1.3-0.8-3,0.2-3,1.7v4.8c0,1.5,1.7,2.5,3,1.7l4.2-2.4C91.5,7.4,91.5,5.5,90.2,4.7z"></path>
</g>
</svg>
</a>

<div class="hs-unfold">
<a class="js-hs-unfold-invoker btn btn-icon btn-xs btn-soft-secondary" href="javascript:;" data-hs-unfold-options='{
                                    "target": "#sidebarContent",
                                    "type": "css-animation",
                                    "animationIn": "fadeInLeft",
                                    "animationOut": "fadeOutLeft",
                                    "hasOverlay": "rgba(55, 125, 255, 0.1)",
                                    "smartPositionOff": true
                                }'>
<svg width="10" height="10" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
<path fill="currentColor" d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z" />
</svg>
</a>
</div>
</div>

 
<div class="scrollbar sidebar-body">
<div class="sidebar-content py-4">

<div class="">
<div id="sidebarNavExample3" class="collapse show navbar-collapse">

<div class="sidebar-body_inner">
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
Home
</a>
<div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav1" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../home/index.html">Home v1</a>
<a class="dropdown-item" href="../home/home-v2.html">Home v2</a>
<a class="dropdown-item" href="../home/home-v3.html">Home v3</a>
<a class="dropdown-item" href="../home/home-v4.html">Home v4</a>
<a class="dropdown-item" href="../home/home-v5.html">Home v5</a>
<a class="dropdown-item" href="../home/home-v6.html">Home v6 - Vodi Prime (Light)</a>
<a class="dropdown-item" href="../home/home-v7.html">Home v7 - Vodi Prime (Dark)</a>
<a class="dropdown-item" href="../home/home-v8.html">Home v8 - Vodi Stream</a>
<a class="dropdown-item" href="../home/home-v9.html">Home v9 - Vodi Tube (Light)</a>
<a class="dropdown-item" href="../home/home-v10.html">Home v10 - Vodi Tube (Dark)</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Collapse" data-target="#sidebarNav2Collapse">
Archive Pages
</a>
<div id="sidebarNav2Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav2" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../archive/movies.html">Movies</a>
<a class="dropdown-item" href="../archive/tv-shows.html">TV Shows</a>
<a class="dropdown-item" href="../archive/videos.html">Videos</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2One">
Single Movies
</a>
<div id="sidebarNav2One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-movies/single-movies-v1.html">Movie v1</a>
<a class="dropdown-item" href="../single-movies/single-movies-v2.html">Movie v2</a>
<a class="dropdown-item" href="../single-movies/single-movies-v3.html">Movie v3</a>
<a class="dropdown-item" href="../single-movies/single-movies-v4.html">Movie v4</a>
<a class="dropdown-item" href="../single-movies/single-movies-v5.html">Movie v5</a>
<a class="dropdown-item" href="../single-movies/single-movies-v6.html">Movie v6</a>
<a class="dropdown-item" href="../single-movies/single-movies-v7.html">Movie v7</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav2Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav2Two">
Single Videos
</a>
<div id="sidebarNav2Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav2">
<a class="dropdown-item" href="../single-video/single-video-v1.html">Video v1</a>
<a class="dropdown-item" href="../single-video/single-video-v2.html">Video v2</a>
<a class="dropdown-item" href="../single-video/single-video-v3.html">Video v3</a>
<a class="dropdown-item" href="../single-video/single-video-v4.html">Video v4</a>
<a class="dropdown-item" href="../single-video/single-video-v5.html">Video v5</a>
<a class="dropdown-item" href="../single-video/single-video-v6.html">Video v6</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Collapse" data-target="#sidebarNav3Collapse">
Single Episodes
</a>
<div id="sidebarNav3Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav3" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../single-episodes/single-episodes-v1.html">Episode v1</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v2.html">Episode v2</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v3.html">Episode v3</a>
<a class="dropdown-item" href="../single-episodes/single-episodes-v4.html">Episode v4</a>
</div>
 </div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3One">
Other Pages
</a>
<div id="sidebarNav3One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../other/landing-v1.html">Landing v1</a>
<a class="dropdown-item" href="../other/landing-v2.html">Landing v2</a>
<a class="dropdown-item" href="../other/coming-soon.html">Coming Soon</a>
<a class="dropdown-item" href="../single-tv-show/single-tv-show.html">Single TV Show</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav3Two" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav3Two">
Blog Pages
</a>
<div id="sidebarNav3Two" class="navbar-nav flex-column collapse" data-parent="#sidebarNav3">
<a class="dropdown-item" href="../blog/blog.html">Blog</a>
<a class="dropdown-item" href="../blog/blog-single.html">Single Blog</a>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4Collapse" data-target="#sidebarNav4Collapse">
Static Pages
</a>
<div id="sidebarNav4Collapse" class="collapse" data-parent="#sidebarNavExample3">
<div id="sidebarNav4" class="navbar-nav align-items-start flex-column">
<a class="dropdown-item" href="../static/contact.html">Contact Us</a>
<a class="dropdown-item" href="../static/404.html">404</a>
</div>
</div>
</div>
<div class="position-relative">
<a class="dropdown-nav-link dropdown-toggle dropdown-toggle-collapse" href="javascript:;" data-target="#sidebarNav4One" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav4One">
Docs
</a>
<div id="sidebarNav4One" class="navbar-nav flex-column collapse" data-parent="#sidebarNav4">
<a class="dropdown-item" href="../../documentation/index.html">Documentation</a>
<a class="dropdown-item" href="../../snippets/index.html">Snippets</a>
</div>
 </div>

</div>
</div>
</div>

</div>
</div>

</div>
</div>
</div>
</aside>



<a class="js-go-to go-to position-fixed" href="javascript:;" style="visibility: hidden;" data-hs-go-to-options='{
            "offsetTop": 700,
            "position": {
                "init": {
                    "right": 15
                },
                "show": {
                    "bottom": 15
                },
                "hide": {
                    "bottom": -15
                }
            }
        }'>
<i class="fas fa-angle-up"></i>
</a>
<?php include_once Config::path()->INCLUDE_PATH.'/oceanfrontscript.php'; ?>

</body>
</html>


